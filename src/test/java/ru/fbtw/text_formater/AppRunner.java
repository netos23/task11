package ru.fbtw.text_formater;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import ru.fbtw.text_fomater.App;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;

public class AppRunner {

	private static final String OUTPUT_PATH = "test_output";
	private static final String TEST_PATH = "tests";

	@BeforeAll
	static void setup() {
		File file = new File(OUTPUT_PATH);
		if (file.exists()) {
			file.delete();
			file.mkdir();
		} else {
			file.mkdir();
		}
	}

	private static Stream<File> fileProvider() {
		File file = new File(TEST_PATH);
		return Arrays.stream(Objects.requireNonNull(file.listFiles()));
	}

	@ParameterizedTest
	@MethodSource("fileProvider")
	void run(File test) {
		String output = setupTestFolder(test.getName());
		App.main( "-i", test.getAbsolutePath(), "-o", output);
	}

	String setupTestFolder(String name) {
		String dirname = name.substring(0, name.length() - 4);
		File testOutput = new File(OUTPUT_PATH + "/" + dirname);
		testOutput.mkdir();
		return testOutput.getAbsolutePath() + "\\" + name;
	}


}
