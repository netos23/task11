package ru.fbtw.text_fomater.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TextFormatter {
	private static final String PREFIX = "      ";
	public static final int MIN_LEN = 9;

	public static String formatDiv(String div, int width) {
		div = div.trim();
		LinkedList<String> queue = new LinkedList<>();
		Collections.addAll(queue, div.split(" "));

		if (queue.isEmpty()) {
			return "";
		}
		getFirstWord(queue, width);
		StringBuilder resultBuilder = new StringBuilder();
		int index = 0;
		List<String> line = new ArrayList<>();

		while (!queue.isEmpty()) {
			String word = queue.pollFirst();

			if (word.isEmpty()) {
				continue;
			}

			int nextIndex = index + word.length();
			if (index != 0) nextIndex++;

			if (nextIndex <= width) {
				line.add(word);
				if (index != 0) {
					index++;
				}
				index += word.length();
			} else {
				if (index == 0) {
					splitWord(queue, width, word);
				} else {
					queue.addFirst(word);
					String formatedLine = buildLine(index, line, width);
					line.clear();
					resultBuilder.append(formatedLine);
					index = 0;
				}
			}
		}

		if (index != 0) {
			String formatedLine = buildLine(index, line, width);
			line.clear();
			resultBuilder.append(formatedLine);
		}

		return resultBuilder.toString();
	}

	private static String buildLine(int index, List<String> line, int width) {
		int addSpaces = width - index;
		int holeCount = line.size() == 1 ? 1 : line.size() - 1;
		int spaceCount = addSpaces / holeCount;
		StringBuilder lineBuilder = new StringBuilder();
		for (int i = 0, lineSize = line.size(); i < lineSize; i++) {
			String word = line.get(i);
			lineBuilder.append(word);

			if (i != lineSize - 1) {
				lineBuilder.append(' ');

				if (addSpaces - spaceCount >= 0) {
					for (int j = 0; j < spaceCount; j++) {
						lineBuilder.append(' ');
					}
					addSpaces -= spaceCount;
				} else {
					for (int j = 0; j < addSpaces; j++) {
						lineBuilder.append(' ');
					}
					addSpaces = 0;
				}
			}
		}
		lineBuilder.append('\n');
		return lineBuilder.toString();
	}

	private static void getFirstWord(LinkedList<String> queue, int width) {
		String firstWord = PREFIX + queue.pollFirst();
		if (firstWord.length() > width) {
			splitWord(queue, width, firstWord);
		} else {
			queue.addFirst(firstWord);
		}
	}

	private static void splitWord(LinkedList<String> queue, int width, String word) {
		String[] strings = splitWord(word, width);
		queue.addFirst(strings[1]);
		queue.addFirst(strings[0]);
	}


	private static String[] splitWord(String word, int len) {
		return new String[]{word.substring(0, len), word.substring(len)};
	}
}
