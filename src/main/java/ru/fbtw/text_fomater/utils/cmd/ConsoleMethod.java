package ru.fbtw.text_fomater.utils.cmd;

@FunctionalInterface
public interface ConsoleMethod {
	void execute(String ... args) throws Exception;
}
