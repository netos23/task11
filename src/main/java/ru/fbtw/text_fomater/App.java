package ru.fbtw.text_fomater;

import ru.fbtw.text_fomater.core.TextFormatter;
import ru.fbtw.text_fomater.utils.cmd.ConsoleArgInputProcessor;
import ru.fbtw.text_fomater.utils.tmp.ExceptionMessages;

import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
	private static boolean isConsoleInput = true;
	private static boolean isConsoleOutput = true;
	private static PrintStream output = System.out;
	private static Scanner input = new Scanner(System.in);

	/**
	 * Метод переопределяет ввод с консоли на ввод с файла
	 *
	 * @param args - в параметре 0 содержиться путь к файлу который следует загрузить
	 * @throws FileNotFoundException
	 * @throws InputMismatchException
	 */
	private static void setInput(String... args) throws FileNotFoundException, InputMismatchException {
		try {
			String filename = args[0];
			isConsoleInput = false;

			if (validateFileName(filename)) {

				File inputFile = new File(filename);
				input = new Scanner(inputFile);
			} else {
				throw new InputMismatchException(ExceptionMessages.FILE_TYPE_ERR);
			}

		} catch (FileNotFoundException ex) {
			throw new FileNotFoundException(ExceptionMessages.FILE_ERR);
		}

	}

	/**
	 * @param fileName - имя файло которое следует проверить
	 * @return - возвращает true если фаил является текстовым
	 */
	private static boolean validateFileName(String fileName) {
		return fileName.endsWith(".txt");
	}

	/**
	 * Метод переопределяет вывод в консоли на вывод в файл
	 *
	 * @param args - в параметре 0 содержиться путь к файлу в который следует сохранить
	 * @throws FileNotFoundException
	 * @throws InputMismatchException
	 */
	private static void setOutput(String... args) throws FileNotFoundException, InputMismatchException {
		try {
			String filename = args[0];
			isConsoleOutput = false;

			if (validateFileName(filename)) {
				File outputFile = new File(filename);
				output = new PrintStream(outputFile);

			} else {
				throw new InputMismatchException(ExceptionMessages.FILE_TYPE_ERR);
			}

		} catch (FileNotFoundException ex) {
			throw new FileNotFoundException(ExceptionMessages.FILE_ERR);
		}
	}

	/**
	 * Просмотр помощи
	 *
	 * @param args - аргументы отсутствуют
	 */
	private static void getHelp(String... args) {
		String message = "Добро пожаловать в программу форматирования текста!\n" +
				"Программа позволяет форматировать текст)\n" +
				"список команд: \n" +
				"--help запросить помощь\n" +
				"-i [dir] указать деррикторию из которой следует загрузить данные,\n" +
				"\tесли в headless режиме флаг не указан, то ввод необходимо произвести с консоли\n" +
				"-o [dir] указать деррикторию в которую следует сохранить данные,\n" +
				"\tесли в headless режиме флаг не указан, то вывод будет произведен в консоль\n";
		System.out.println(message);
		System.exit(0);
	}

	public static void main(String... args) {
		ConsoleArgInputProcessor processor = new ConsoleArgInputProcessor();

		processor.setFlagEvent("--help", App::getHelp)
				.setFlagEvent("-i", App::setInput)
				.setFlagEvent("-o", App::setOutput);

		try {
			processor.execute(args);
			if (isConsoleInput) {
				throw new Exception(ExceptionMessages.INPUT_ERR);
			}
			convert();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	private static void convert() throws NumberFormatException, InputMismatchException {
		if(isConsoleOutput){
			System.out.println("Форматированный текст:");
		}
		try {
			int width = Integer.parseInt(input.nextLine().trim());
			if(width<TextFormatter.MIN_LEN){
				throw new InputMismatchException("Слишком маленькая ширина");
			}

			while (input.hasNext()) {
				String line = input.nextLine();
				output.print(TextFormatter.formatDiv(line, width));
			}
		} catch (NumberFormatException ex) {
			throw new NumberFormatException(ExceptionMessages.INPUT_ERR);
		}
	}
}
